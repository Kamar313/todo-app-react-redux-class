// import React from "react";
// import { useState } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { addTodo, deleteitem, compledTask, searchItem } from "./store/Action";
// function App() {
//   const [todo, setTodo] = useState("");
//   const despatch = useDispatch();
//   const addTodo1 = useSelector((state) => state.todo);

//   const handel = (e) => {
//     setTodo(e.target.value);
//   };

//   const click = (e) => {
//     if (e.key == "Enter" && e.target.value != "") {
//       despatch(addTodo(todo));
//       setTodo("");
//     }
//   };
//   const deleteDAta = (id) => {
//     despatch(deleteitem(id));
//   };
//   const complete = (id) => {
//     despatch(compledTask(id));
//   };
//   const search = (value) => {
//     despatch(searchItem(value));
//   };

//   return (
//     <>
//       <div className=" w-1/1 h-screen bg-slate-500 flex justify-center items-center">
//         <div className=" w-2/6 h-5/6 min-h-0 bg-white rounded-lg text-center relative">
//           <h1 className="text-center text-5xl">TODO App</h1>
//           <input
//             type="text"
//             placeholder="Search Task Here"
//             onChange={(e) => search(e.target.value)}
//             className="bg-gray-100  h-10 rounded-lg pl-3 mt-4 mb-2 "
//           ></input>
//           <div>
//             <input
//               type="text"
//               placeholder="add Todo"
//               value={todo}
//               onChange={handel}
//               onKeyDown={click}
//               className="bg-gray-100  h-10 rounded-lg pl-3 "
//             ></input>
//           </div>
//           <div>
//             {addTodo1.list.map((ele, i) => {
//               return (
//                 <div
//                   key={`div ${i}`}
//                   className={
//                     ele.search == "Find"
//                       ? " flex h-12 w-1/1 relative border-b-2 items-center"
//                       : " hidden"
//                   }
//                 >
//                   <input
//                     type="checkbox"
//                     onClick={() => complete(ele.id)}
//                     key={`i${i}`}
//                     className="ml-2"
//                   ></input>
//                   <p
//                     key={`p${i}`}
//                     className={
//                       ele.state == "completed"
//                         ? "inline line-through text-ellipsis w-4/5 text-start ml-2"
//                         : "text-ellipsis w-4/5 text-start ml-2"
//                     }
//                   >
//                     {ele.task}
//                   </p>
//                   <button onClick={() => deleteDAta(ele.id)} key={`btn${i}`}>
//                     <img src="./icons8-remove-48.webp" rel="delet" />
//                   </button>
//                 </div>
//               );
//             })}
//           </div>
//         </div>
//       </div>
//     </>
//   );
// }

// export default App;

import React, { Component } from "react";
import { connect } from "react-redux";
import { addTodo, deleteitem, compledTask, searchItem } from "./store/Action";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todo: "",
    };
  }

  handel = (e) => {
    this.setState({ todo: e.target.value });
  };

  click = (e) => {
    if (e.key == "Enter" && e.target.value != "") {
      this.props.addTodo(this.state.todo);
      this.setState({ todo: (e.target.value = "") });
    }
  };
  deleteDAta = (id) => {
    this.props.deleteitem(id);
  };
  complete = (id) => {
    this.props.compledTask(id);
  };
  search = (value) => {
    this.props.searchItem(value);
  };

  render() {
    return (
      <>
        <div className=" w-1/1 h-screen bg-slate-500 flex justify-center items-center">
          <div className=" w-2/6 h-5/6 min-h-0 bg-white rounded-lg text-center relative">
            <h1 className="text-center text-5xl">TODO App</h1>
            <input
              type="text"
              placeholder="Search Task Here"
              onChange={(e) => this.search(e.target.value)}
              className="bg-gray-100  h-10 rounded-lg pl-3 mt-4 mb-2 "
            ></input>
            <div>
              <input
                type="text"
                placeholder="add Todo"
                value={this.state.value}
                onChange={(e) => this.handel(e)}
                onKeyDown={(e) => this.click(e)}
                className="bg-gray-100  h-10 rounded-lg pl-3 "
              ></input>
            </div>

            <div>
              {this.props.todoDataget.list.map((ele, i) => {
                return (
                  <div
                    key={`div ${i}`}
                    className={
                      ele.search == "Find"
                        ? " flex h-12 w-1/1 relative border-b-2 items-center"
                        : " hidden"
                    }
                  >
                    <input
                      type="checkbox"
                      onClick={() => this.complete(ele.id)}
                      key={`i${i}`}
                      className="ml-2"
                    ></input>
                    <p
                      key={`p${i}`}
                      className={
                        ele.state == "completed"
                          ? "inline line-through text-ellipsis w-4/5 text-start ml-2"
                          : "text-ellipsis w-4/5 text-start ml-2"
                      }
                    >
                      {ele.task}
                    </p>
                    <button
                      onClick={() => this.deleteDAta(ele.id)}
                      key={`btn${i}`}
                    >
                      <img src="./icons8-remove-48.webp" rel="delet" />
                    </button>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return { todoDataget: state.todo };
}
export default connect(mapStateToProps, {
  addTodo,
  deleteitem,
  compledTask,
  searchItem,
})(App);
