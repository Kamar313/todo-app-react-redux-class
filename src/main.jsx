import React from "react";
import ReactDOM from "react-dom/client";

import Reducer from "./reducer";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Reducer />
  </React.StrictMode>
);
